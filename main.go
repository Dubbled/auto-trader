package main

import (
	"gitlab.com/dubbled/auto-trade/api"
	"net/http"
)

func main() {
	go http.ListenAndServe(":8081", http.FileServer(http.Dir("./app/build/")))
	api.Listen("8080")
}
