package bitstamp

import (
	"encoding/json"
	"gitlab.com/dubbled/auto-trade/exchange"
	"io/ioutil"
	"net/http"
	"strconv"
)

const (
	url = "https://www.bitstamp.net/api/v2/"
)

type Ticker struct {
	Last      string `json:"last"`
	High      string `json:"high"`
	Low       string `json:"low"`
	VWAP      string `json:"vwap"`
	Volume    string `json:"volume"`
	Sell      string `json:"bid"`
	Buy       string `json:"ask"`
	Timestamp string `json:"timestamp"`
	Open      string `json:"open"`
}

type Trade struct {
	Date   string `json:"date"`
	ID     string `json:"tid"`
	Price  string `json:"price"`
	Kind   string `json:"type"`
	Amount string `json:"amount"`
}

func GetTicker() (*exchange.Ticker, error) {
	query := url + "ticker/btcusd"
	resp, err := http.Get(query)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	t := &Ticker{}
	err = json.Unmarshal(body, t)
	if err != nil {
		return nil, err
	}

	e := &exchange.Ticker{}
	time, err := strconv.ParseInt(t.Timestamp, 10, 64)
	if err != nil {
		return nil, err
	}
	sell, err := strconv.ParseFloat(t.Sell, 32)
	if err != nil {
		return nil, err
	}
	vwap, err := strconv.ParseFloat(t.VWAP, 32)
	if err != nil {
		return nil, err
	}
	buy, err := strconv.ParseFloat(t.Buy, 32)
	if err != nil {
		return nil, err
	}
	last, err := strconv.ParseFloat(t.Last, 32)
	if err != nil {
		return nil, err
	}
	volume, err := strconv.ParseFloat(t.Volume, 32)
	if err != nil {
		return nil, err
	}

	e.Time = time
	e.VWAP = float32(vwap)
	e.Buy = float32(buy)
	e.Sell = float32(sell)
	e.Last = float32(last)
	e.Volume = float32(volume)
	e.Average = float32((buy + sell) / 2)
	return e, nil
}

func GetOrders() ([]exchange.Order, error) {
	query := url + "order_book/btcusd"
	resp, err := http.Get(query)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	type Response struct {
		Time string     `json:"timestamp"`
		Bids [][]string `json:"bids"`
		Asks [][]string `json:"asks"`
	}

	r := &Response{}
	err = json.Unmarshal(body, r)
	if err != nil {
		return nil, err
	}

	orders := make([]exchange.Order, len(r.Bids)+len(r.Asks))

	for i, b := range r.Bids {
		z := exchange.Order{}
		z.Kind = "bid"
		price, err := strconv.ParseFloat(b[1], 32)
		if err != nil {
			continue
		}
		amount, err := strconv.ParseFloat(b[0], 32)
		if err != nil {
			continue
		}
		z.Price = float32(price)
		z.Amount = float32(amount)
		orders[i] = z
	}

	for i, a := range r.Asks {
		z := exchange.Order{}
		z.Kind = "ask"
		price, err := strconv.ParseFloat(a[0], 32)
		if err != nil {
			continue
		}
		amount, err := strconv.ParseFloat(a[1], 32)
		if err != nil {
			continue
		}
		z.Price = float32(price)
		z.Amount = float32(amount)
		orders[i+len(r.Bids)] = z
	}

	return orders, nil
}

func GetTrades() ([]exchange.Trade, error) {
	query := url + "transactions/btcusd"
	resp, err := http.Get(query)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var apiTrades []Trade
	err = json.Unmarshal(body, &apiTrades)
	if err != nil {
		return nil, err
	}

	trades := make([]exchange.Trade, len(apiTrades))
	for i, t := range apiTrades {
		z := exchange.Trade{}
		if t.Kind == "0" {
			z.Kind = "bid"
		} else {
			z.Kind = "ask"
		}

		price, err := strconv.ParseFloat(t.Price, 32)
		if err != nil {
			continue
		}
		amount, err := strconv.ParseFloat(t.Amount, 32)
		if err != nil {
			continue
		}
		time, err := strconv.ParseInt(t.Date, 10, 64)
		if err != nil {
			continue
		}
		z.Time = time
		z.Price = float32(price)
		z.Amount = float32(amount)
		z.Exchange = "Bitstamp"
		trades[i] = z
	}
	return trades, nil
}
