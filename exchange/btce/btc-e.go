package btce

import (
	"encoding/json"
	"gitlab.com/dubbled/auto-trade/exchange"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	url = "https://btc-e.com/api/3/"
)

type Info struct {
	DecimalPlaces int     `json:"decimal_places"`
	MinPrice      float32 `json:"min_price"`
	MaxPrice      float32 `json:"max_price"`
	MinAmount     float32 `json:"min_amount"`
	Fee           float32 `json:"fee"`
}

type Ticker struct {
	High           float32 `json:"high"`
	Low            float32 `json:"low"`
	Average        float32 `json:"avg"`
	Volume         float32 `json:"vol"`
	CurrencyVolume float32 `json:"vol_cur"`
	Last           float32 `json:"last"`
	Buy            float32 `json:"buy"`
	Sell           float32 `json:"sell"`
	Time           int64   `json:"updated"`
}

type Orders struct {
	Asks []Order `json:"asks"`
	Bids []Order `json:"bids"`
}

type Order []float32

type Trade struct {
	Kind   string  `json:"type"`
	Price  float32 `json:"price"`
	Amount float32 `json:"amount"`
	Time   int64   `json:"timestamp"`
	ID     int64   `json:"tid"`
}

func GetInfo() (*exchange.Info, error) {
	query := url + "info/btc_usd"
	resp, err := http.Get(query)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	type Response struct {
		Time  int64           `json: server_time`
		Pairs map[string]Info `json: pairs`
	}

	r := &Response{}

	err = json.Unmarshal(body, r)
	if err != nil {
		return nil, err
	}
	e := &exchange.Info{}
	e.Time = time.Unix(r.Time, 0)
	e.Fee = r.Pairs["btc_usd"].Fee
	return e, nil
}

func GetTicker() (*exchange.Ticker, error) {
	query := url + "ticker/btc_usd"

	resp, err := http.Get(query)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	type Response struct {
		BTC_USD Ticker `json:"btc_usd"`
	}

	r := &Response{}

	err = json.Unmarshal(body, r)
	if err != nil {
		return nil, err
	}

	t := r.BTC_USD

	e := &exchange.Ticker{}

	e.Time = t.Time
	e.Average = t.Average
	e.Buy = t.Buy
	e.Sell = t.Sell
	e.Volume = t.Volume
	e.Last = t.Last
	e.CurrencyVolume = t.CurrencyVolume

	return e, nil
}

func GetOrders() ([]exchange.Order, error) {
	query := url + "depth/btc_usd"
	resp, err := http.Get(query)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	type Response struct {
		Os Orders `json:"btc_usd"`
	}

	r := &Response{}

	err = json.Unmarshal(body, r)
	if err != nil {
		return nil, err
	}

	orders := make([]exchange.Order, 0, len(r.Os.Asks)+len(r.Os.Bids))
	for _, a := range r.Os.Asks {
		z := exchange.Order{}
		z.Kind = "ask"
		z.Price = a[0]
		z.Amount = a[1]
		orders = append(orders, z)
	}

	for _, b := range r.Os.Bids {
		z := exchange.Order{}
		z.Kind = "bid"
		z.Price = b[0]
		z.Amount = b[1]
		orders = append(orders, z)
	}

	return orders, nil
}

func GetTrades() ([]exchange.Trade, error) {
	query := url + "trades/btc_usd"
	resp, err := http.Get(query)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	type Response struct {
		Trades []Trade `json:"btc_usd"`
	}

	r := &Response{}

	err = json.Unmarshal(body, r)
	if err != nil {
		return nil, err
	}

	trades := make([]exchange.Trade, len(r.Trades))
	for i, t := range r.Trades {
		z := exchange.Trade{}
		z.Kind = t.Kind
		z.Price = t.Price
		z.Amount = t.Amount
		z.Time = t.Time
		z.Exchange = "BTC-E"
		trades[i] = z
	}
	return trades, nil
}
