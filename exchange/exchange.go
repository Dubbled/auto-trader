package exchange

import (
	"time"
)

type Info struct {
	Fee  float32
	Time time.Time
}

type Ticker struct {
	Average        float32
	Buy            float32
	Sell           float32
	Volume         float32
	CurrencyVolume float32
	VWAP           float32
	Last           float32
	Time           int64
}

type Order struct {
	Kind   string
	Price  float32
	Amount float32
}

type Trade struct {
	Kind     string
	Price    float32
	Amount   float32
	Exchange string
	Time     int64
}
