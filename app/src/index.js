import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import './bootstrap.min.css'
import Nav from './components/Nav'
import Main from './components/layouts/Main'
import { BrowserRouter } from 'react-router-dom'

class App extends React.Component {
	render() {
		return (
			<div className="app">
				<Nav/>
				<Main/>
			</div>
		)
	}
}


ReactDOM.render(
	<BrowserRouter>
		<App />
	</BrowserRouter>, document.getElementById('root'));
