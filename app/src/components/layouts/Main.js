import React from 'react'
import { Route, Switch } from 'react-router-dom'
import TradeList from '../TradeList'
import TradeChart from '../TradeChart'
import Ticker from '../Ticker'

class Header extends React.Component {
  render() {
	return (
	<header>
		<div className="container-fluid">
			<div className="row">
				<div className="col-md-6 ticker">
					<span>Bitstamp</span>
					<Ticker exchange="bitstamp"/>
				</div>
				<div className="col-md-6 ticker">
					<span>BTC-E</span>
					<Ticker exchange="btc-e"/>
				</div>
			</div>
		</div>
	</header>
	)
  }
}

class Sidebar extends React.Component {
	render() {
		return (<h3></h3>)
	}

}

class Trades extends React.Component {
	render() {
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-2 sidebar">
						<Sidebar/>
					</div>
					<div className="col-md-10">
						<Header/>
                        <TradeChart/>
						<div className="main">
							<TradeList/>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

class Main extends React.Component {
	render() {
		return (
			<div className="" id="page-content-wrapper">
				<Switch>
					<Route exact path='/' component={Trades}/>
				</Switch>
			</div>
		)
	}
}

export default Main;
