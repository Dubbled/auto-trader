import React from 'react';
import Request from 'superagent';

class TradeList extends React.Component {
	constructor() {
		super();
		this.state = {
			offset: 0,
			limit: 50,
			currentPage: 0,
			numPages: 1,
			trades: [],
		};
	}

	componentWillMount() {
		this.grabTrades();
	}

	constructQuery() {
		return "http://localhost:8080/trades?limit=" + this.state.limit + "?offset=" + this.state.currentPage * this.state.limit;
	}

	grabTrades() {
		var query = this.constructQuery();
		Request.get(query).end((err, res) => {
			if (err || !res.ok) {
				console.log(err);
			} else {
				var data = JSON.parse(res.text);
				this.setState({
					trades: data.trades, 
					numPages: Math.ceil(data.num / this.state.limit)
				});
			}
		});
	}

	nextPage() {
		if (this.state.currentPage < this.state.numPages-1) {
			this.setState({
				currentPage: this.state.currentPage + 1
			},
			function() {
				this.grabTrades();			
			})
		}
	}

	prevPage() {
		if (this.state.currentPage > 0) {
			this.setState({
				currentPage: this.state.currentPage - 1
			},
			function() {
				this.grabTrades();
			})
		}
	}

	render() {
		var list = this.state.trades.map(function(trade, index) {
				if (trade.Amount === 0) {
					return "";
				}
				return <tr key={index}><td>{trade.Kind}</td><td>{trade.Price}</td>
				<td>{trade.Amount}</td><td>{trade.Exchange}</td></tr>;
			}	
		);
		return (
		<div className="container-fluid">
			<div className="row">
				<table className="table table-hover table-condensed">
					<thead>
						<tr><td>Type</td><td>Price</td><td>Amount</td><td>Exchange</td></tr>
					</thead>
					<tbody>
						{list}
					</tbody>
				</table>
			</div>
			<div className="pagers">
				<div className="row paging">
					<div className="col-md-3 page-btn"><button onClick={() => {this.prevPage()}}><i className="fa fa-chevron-left"></i></button></div>
					<div className="col-md-6 page-btn">{this.state.currentPage + 1} / {this.state.numPages}</div>
					<div className="col-md-3 page-btn"><button onClick={() => {this.nextPage()}}><i className="fa fa-chevron-right"></i></button></div>
				</div>
			</div>
		</div>
		);
	}
}

export default TradeList;
