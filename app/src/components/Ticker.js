import React from 'react'
import PropTypes from 'prop-types'
import Request from 'superagent'

class Ticker extends React.Component {
  constructor() {
    super();
    this.state = {
      ticker: {}
    };
  }

  componentWillMount() {
    this.getTicker();
  }
  getTicker() {
    var url = "http://localhost:8080/ticker/" + this.props.exchange
  	Request.get(url).end((err, res) => {
  		if (err || !res.ok) {
  			console.log(err);
  		} else {
  			var data = JSON.parse(res.text);
  			this.setState({ticker:data});
  		}
  	});
  }

  render() {
    return (
        <div className="ticker-value">${this.state.ticker.Average}</div>
    );
  }
}

Ticker.propTypes = {
    exchange: PropTypes.string.isRequired,
};

export default Ticker;