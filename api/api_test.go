package api

import (
	"testing"
)

func TestParameterParse(t *testing.T) {
	r := []int{0, 1, 2, 3, 4, 5}
	_, err := parseParams("offset=2", len(r))
	if err != nil {
		t.Fail()
	}
	_, err = parseParams("limit=3?offset=2", len(r))
	if err != nil {
		t.Fail()
	}
	_, err = parseParams("limit=4?offset=2234", len(r))
	if err == nil {
		t.Fail()
	}
}
