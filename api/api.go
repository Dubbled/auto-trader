package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/pressly/chi"
	"gitlab.com/dubbled/auto-trade/exchange"
	"gitlab.com/dubbled/auto-trade/exchange/bitstamp"
	"gitlab.com/dubbled/auto-trade/exchange/btce"
	"net/http"
	"strconv"
	"strings"
)

type Parameters struct {
	Limit  int
	Offset int
}

func Listen(port string) {
	r := chi.NewRouter()

	r.Route("/trades", func(r chi.Router) {
		r.Get("/", GetAllTrades)
		r.Route("/:exchange", func(r chi.Router) {
			r.Get("/", GetTrades)
		})
	})

	r.Route("/ticker", func(r chi.Router) {
		r.Get("/", GetAllTickers)
		r.Route("/:exchange", func(r chi.Router) {
			r.Get("/", GetTicker)
		})
	})

	http.ListenAndServe(fmt.Sprintf(":%s", port), r)
}

// Function takes in a URL's RawQuery, and parses it into a parameter struct pointer.
func parseParams(query string, retLength int) (*Parameters, error) {
	if len(query) == 0 {
		return nil, nil
	}
	split := strings.Split(query, "?")
	p := &Parameters{}
	for i := 0; i < len(split); i++ {
		switch strings.ToLower(split[i][:strings.Index(split[i], "=")]) {
		case "limit":
			val, err := strconv.ParseInt(split[i][strings.Index(split[i], "=")+1:], 10, 32)
			if err != nil {
				return nil, err
			}
			p.Limit = int(val)
			break
		case "offset":
			val, err := strconv.ParseInt(split[i][strings.Index(split[i], "=")+1:], 10, 32)
			if err != nil {
				return nil, err
			}
			p.Offset = int(val)
			break
		default:
			return nil, errors.New(fmt.Sprintf("Invalid Query Param: %s", split[i][:strings.Index(split[i], "=")]))
			break
		}
	}

	if p.Limit < 0 {
		return nil, errors.New("Bad limit parameter.")
	}

	if p.Offset < 0 || p.Offset > retLength {
		return nil, errors.New("Bad offset parameter.")
	}

	if p.Limit == 0 {
		p.Limit = 50
	}

	if p.Limit+p.Offset > retLength {
		p.Limit = p.Limit - ((p.Offset + p.Limit) - retLength)
	}

	return p, nil
}

func GetAllTickers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	tickers := make(map[string]float32)

	btce, err := btce.GetTicker()
	if err != nil {
		return
	}

	bitstamp, err := bitstamp.GetTicker()
	if err != nil {
		return
	}

	tickers["BTC-E"] = btce.Average
	tickers["Bitstamp"] = bitstamp.Average

	body, err := json.Marshal(tickers)
	if err != nil {
		return
	}

	w.Write(body)
}

func GetTicker(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	ex := chi.URLParam(r, "exchange")
	var t *exchange.Ticker
	var err error
	switch ex {
	case "bitstamp":
		t, err = bitstamp.GetTicker()
	case "btc-e":
		t, err = btce.GetTicker()
	default:
		w.Write([]byte("Invalid exchange name.\n"))
		return
	}
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	body, err := json.Marshal(t)
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}
	w.Write(body)
}

func GetAllTrades(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	trades := make([]exchange.Trade, 0, 50)
	t1, err := bitstamp.GetTrades()
	if err != nil {
		w.Write([]byte("Failed to get btc-e trades.\n"))
		return
	}
	t2, err := btce.GetTrades()
	if err != nil {
		w.Write([]byte("Failed to get bitstamp trades.\n"))
		return
	}

	for _, t := range t1 {
		trades = append(trades, t)
	}

	for _, t := range t2 {
		trades = append(trades, t)
	}

	p, err := parseParams(r.URL.RawQuery, len(trades))
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	var t []exchange.Trade
	if p != nil {
		t = trades[p.Offset : p.Limit+p.Offset]
	} else {
		t = trades
	}

	body, err := json.Marshal(struct {
		Items  int              `json:"num"`
		Trades []exchange.Trade `json:"trades"`
	}{
		len(trades),
		t,
	})
	if err != nil {
		w.Write([]byte("Failed to marshal trades.\n"))
		return
	}
	w.Write(body)
}

func GetTrades(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	ex := chi.URLParam(r, "exchange")
	var trades []exchange.Trade
	var err error
	switch ex {
	case "bitstamp":
		trades, err = bitstamp.GetTrades()
		if err != nil {
			w.Write([]byte("Failed to get trades.\n"))
			return
		}
	case "btc-e":
		trades, err = btce.GetTrades()
		if err != nil {
			w.Write([]byte("Failed to get trades.\n"))
			return
		}
	default:
		w.Write([]byte("Invalid exchange name.\n"))
		return
	}

	p, err := parseParams(r.URL.RawQuery, len(trades))
	if err != nil {
		w.Write([]byte(err.Error()))
		return
	}

	if p != nil {
		trades = trades[p.Offset : p.Offset+p.Limit]
	}

	body, err := json.Marshal(trades)
	if err != nil {
		w.Write([]byte("Failed to marshal trades.\n"))
		return
	}
	w.Write(body)
}
